#%%
from flask import (Flask, render_template, request , redirect, url_for)
# from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from power import my_solar
import config,os,datetime as dt
from apscheduler.schedulers.background import BackgroundScheduler
import pandas as pd
from get_temp import bacoor_temp
import requests

app = Flask(__name__)

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(user=config.POSTGRES_USER,pw=config.POSTGRES_PW,url=config.POSTGRES_URL,db=config.POSTGRES_DB)

def db_exec(statement,mode):
    engine = create_engine(DB_URL)
    engine = engine.execution_options(autocommit=True)
    if mode==0:
        engine.execute(statement)
    elif mode==1:
        return pd.read_sql(statement,engine)    
    engine.dispose()   

def get_solardata():
    # print("Scheduler is alive!")
    exec_time= dt.datetime.now().time()
   # exec_time = dt.time(6,00)
    if exec_time > dt.time(5,00) and exec_time < dt.time(20,00): 
        solar=my_solar("webdriver/chromedriver.exe")
        do=solar.get_numbers()
        temp_now=bacoor_temp()
        if do == 0:
            vals= "now() + interval '8h',{},{},{},{},{}" .format(
                    solar.cp_num
                    # ,solar.cp_unit
                    ,solar.yt_num
                    # ,solar.yt_unit
                    ,solar.ty_num
                    # ,solar.ty_unit
                    ,temp_now
                    ,do
            )
            insert_statement = 'insert into solar.gen_logs values ({})' .format(vals)
            try:
                db_exec(insert_statement,0)
                print('Recorded')
            except:
                print('DB connection failed.')
                pass
        else:
            insert_statement = "insert into solar.gen_logs(collect_dt,rc) values (now() + interval '8h' ,{})" .format(do)
            try:
                db_exec(insert_statement,0)
                print('Recorded')
            except:
                print('DB connection failed.')
                pass      

sched = BackgroundScheduler(daemon=True)
sched.add_job(get_solardata,'interval',minutes=2)
sched.start()


@app.route('/for_siri')
def for_siri():
    collect = "select * from solar.current_usage"
    df_current = db_exec(collect,1)
    load_dttm=df_current.iloc[0]['collect_dt']
    curr=df_current.iloc[0]['curr_num']
    today=df_current.iloc[0]['today_num']

    perc= int(round(curr / 3000,2) * 100)

    msg_greeting = 'Hey Brian!'
    today_tag = 'today ' if dt.date.today() == load_dttm.date() else ''
    msg_date = '\nAs of {}{},' .format(today_tag,load_dttm.strftime('%A %B %d %Y %I:%M %p'))
    msg_cur_gen = ' you are getting {} watts from your solar energy system. This is {}% of your total capability.' .format(curr,perc)
    msg_today_gen = '\nYou have generated a total of {} kilowatt-hour today' .format(today)

    msg_full = msg_greeting + msg_date + msg_cur_gen + msg_today_gen

    return '<article> {} </article>' .format(msg_full)


@app.route('/mini1_on')
def mini1_on():
    data = {}
    data["data"]=	{"switch": "on"}
    url= "http://192.168.1.193:8081/zeroconf/switch"
    r = requests.post(url, json=data)

    return '<article> OK </article>' 

@app.route('/mini1_off')
def mini1_off():
    data = {}
    data["data"]=	{"switch": "off"}
    url= "http://192.168.1.193:8081/zeroconf/switch"
    r = requests.post(url, json=data)

    return '<article> OK </article>' 


# http://192.168.1.15/cm?cmnd=Power%20TOGGLE

# http://<ip>/cm?cmnd=Power%20On
# http://<ip>/cm?cmnd=Power%20off

if __name__ == '__main__':
    app.run(host= '0.0.0.0',port=5000)



# %%
