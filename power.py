from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains

class my_solar:
    def __init__(self,webdrv):
        self.__options = Options()
        self.__options.headless = True
        # self.browser = webdriver.Chrome(executable_path=webdrv, chrome_options=self.__options)
        self.browser = webdriver.Chrome(chrome_options=self.__options)
        self.cp_num = None
        self.cp_unit = None
        self.yt_num = None
        self.yt_unit = None
        self.ty_num = None
        self.ty_unit = None
        self.timeout=20

    def get_numbers(self):
        try:
            self.browser.get('http://admin:admin@192.168.1.123')
        except:
            self.browser.quit()
            return 91

        try:
            status=WebDriverWait(self.browser,self.timeout).until(
                EC.presence_of_element_located((By.ID,"op_t1"))
            )
            ActionChains(self.browser).click(status).perform()
        except :
            self.browser.quit()
            return 92
        try:
            frame=WebDriverWait(self.browser,self.timeout).until(
                EC.presence_of_element_located((By.ID,"child_page"))
            )
            self.browser.switch_to.frame("child_page")
        except :
            self.browser.quit()
            return 93
        try:
            target=WebDriverWait(self.browser,self.timeout).until(
                EC.presence_of_element_located((By.ID,"st4"))
            )
            ActionChains(self.browser).click(target).perform()
        except :
            self.browser.quit()
            return 94

        try:
            data1=WebDriverWait(self.browser,self.timeout).until(
                EC.presence_of_element_located((By.ID,"webdata_now_p"))
            )
            data2=self.browser.find_element_by_id("webdata_today_e")
            data3=self.browser.find_element_by_id("webdata_total_e")
            # data4=self.browser.find_element_by_id("webdata_utime")
           
            self.cp_num, self.cp_unit=data1.text.split(" ")
            self.yt_num, self.yt_unit=data2.text.split(" ")
            self.ty_num, self.ty_unit=data3.text.split(" ")

            # print(data1.text,data2.text,data3.text,data4.text)
        except :
            self.browser.quit()
            return 95


        self.browser.quit()
        return 0
