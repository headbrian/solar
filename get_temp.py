import requests

API_KEY = "8a1f173d45137c87493efef558a6d987"
city_name = "BACOOR"
url = f"http://api.openweathermap.org/data/2.5/weather?q={city_name}&APPID={API_KEY}"




def convert_kelvin_to_celsius(kelvin_temperature):
    return round(kelvin_temperature - 273.15, 2)

def bacoor_temp():
    response = requests.get(url).json()
    current_temperature_kelvin = response['main']['temp']
    current_temp = convert_kelvin_to_celsius(current_temperature_kelvin)
    return current_temp
